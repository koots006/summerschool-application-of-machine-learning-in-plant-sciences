import numpy as np
import matplotlib.pyplot as plt
import open3d as o3d
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import plotly.express as px
import pandas as pd
import os

def load_seedling_pc(directory, plant_id):
  pnt_file = '%s/points/%d.pts' % (directory, plant_id)
  lab_file  = '%s/labels/points_label_semantic/%d.seg' % (directory, plant_id)

  # Check if file exist
  if not os.path.isfile(pnt_file): 
    print("The data for plant id %d is not available" % plant_id)
    return [], []
  
  # Load the data
  # Read the point cloud and the labels
  pc = np.genfromtxt(pnt_file, delimiter=' ',dtype='float32')
  lab = np.genfromtxt(lab_file, delimiter=' ',dtype='int')

  # Change from 4 labels to 2 labels (stem and leaf), to simplify things for the tutorial
  # All labels
  soil_label = 0
  stem_label = 1
  leaf_label = 2
  node_label = 3

  # Remove the soil
  soil = lab == soil_label
  pc = pc[np.invert(soil)]
  lab = lab[np.invert(soil)]
  # Change node to stem
  lab[lab==node_label] = stem_label
  # Make stem 0 and leaf 1
  lab = lab - 1 

  return pc, lab

#def make_labeled_seedling_pcd(pc, lab):
#  pcd = o3d.geometry.PointCloud()
#  pcd.points = o3d.utility.Vector3dVector(pc[:,0:3])
#  max_label = lab.max()
#  colors = plt.get_cmap("Set1")(lab) 
#  colors[lab < 0] = 0
#  pcd.colors = o3d.utility.Vector3dVector(colors[:, :3])
#  return(pcd)

def show_labeled_seedling_pc(points, lab):

  max_label = lab.max()
  colors = plt.get_cmap("Dark2")(lab)
  colors[lab < 0] = 0

  fig = go.Figure(
      data=[go.Scatter3d(x=points[:,0], y=points[:,1], z=points[:,2], mode='markers', marker={'size':2, 'color': colors})],
      layout = {'scene': {'xaxis': {'visible': False},'yaxis': {'visible': False},'zaxis': {'visible': True}, 'aspectmode': 'data'},
                'width': 800, 'height':600}
  )
  fig.show()

def show_pcd(pcd):
  points = np.asarray(pcd.points)
  colors = np.asarray(pcd.colors)

  fig = go.Figure(
      data=[go.Scatter3d(x=points[:,0], y=points[:,1], z=points[:,2], mode='markers', marker={'size':2, 'color': colors})],
      layout = {'scene': {'xaxis': {'visible': False},'yaxis': {'visible': False},'zaxis': {'visible': True}, 'aspectmode': 'data'}, 
                'width': 600, 'height':800}
  )
  fig.show()  


def show_pcds(pcd1, pcd2):
  points1 = np.asarray(pcd1.points)
  colors1 = np.asarray(pcd1.colors)
  points2 = np.asarray(pcd2.points)
  colors2 = np.asarray(pcd2.colors)

  fig = make_subplots(rows=1, cols=2,specs=[[{'is_3d':True},{'is_3d':True}]])
  fig.add_trace( go.Scatter3d(x=points1[:,0], y=points1[:,1], z=points1[:,2], mode='markers', marker={'size':2, 'color': colors1}),                
    row=1, col=1
  )
  fig.add_trace( go.Scatter3d(x=points2[:,0], y=points2[:,1], z=points2[:,2], mode='markers', marker={'size':2, 'color': colors2}),
    row=1, col=2
  )
  fig.update_layout(height=800, width=1200, showlegend=False, 
                    scene= {'xaxis': {'visible': False},'yaxis': {'visible': False},'zaxis': {'visible': True}, 'aspectmode': 'data'},
                    scene2= {'xaxis': {'visible': False},'yaxis': {'visible': False},'zaxis': {'visible': True}, 'aspectmode': 'data'} )
  fig.show()  

def show_pcd_normals(pcd, n=20000, norm_length=20):
  assert pcd.has_normals(), "show_pcd_normals requires that the surface normals have been calculated"

  # Get position and colors of all points 
  all_points = np.asarray(pcd.points)
  all_colors = np.asarray(pcd.colors) 

  # Sample a set of points to show the normals
  n = min(n, len(pcd.points))
  smpl = np.random.permutation(len(pcd.points))[0:n]
  points = all_points[smpl,:]
  normals = np.asarray(pcd.normals)[smpl,:]

  # Scale the length of the normal
  points_end = points + norm_length * normals
  
  # Create arrays with line segments to show the normals. Use nan to separate lines
  normal_data_x = np.ravel(np.column_stack((points[:,0],points_end[:,0],np.full(n, np.nan))))
  normal_data_y = np.ravel(np.column_stack((points[:,1],points_end[:,1],np.full(n, np.nan))))
  normal_data_z = np.ravel(np.column_stack((points[:,2],points_end[:,2],np.full(n, np.nan))))
 
  # Plot the line segment
  fig = make_subplots(rows=1, cols=1,specs=[[{'is_3d':True}]])
  fig = px.line_3d(x=normal_data_x, y=normal_data_y, z=normal_data_z)
  fig.add_trace( go.Scatter3d(x=all_points[:,0], y=all_points[:,1], z=all_points[:,2], mode='markers', marker={'size':2, 'color': all_colors}), row=1, col=1 )
  fig.update_layout(height=800, width=600, showlegend=False, 
                  scene= {'xaxis': {'visible': False},'yaxis': {'visible': False},'zaxis': {'visible': True}, 'aspectmode': 'data'})
  fig.update_traces(connectgaps=False)
  fig.show()

# The labels
stem_label = 0
leaf_label = 1
