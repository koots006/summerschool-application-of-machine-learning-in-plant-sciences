import numpy as np
import matplotlib.pyplot as plt
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import plotly.express as px
import pandas as pd
import os
import pickle
from pathlib import Path
import random
import numpy as np
import h5py

# TensorFlow is a library to implement and run neural networks
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers


NUM_SAMPLE_POINTS = 1024
NUM_FEATURES = 3
BATCH_SIZE = 16
LABELS = ["stemwork", "leaf"]


def get_dataset(dataset_file_path):
    f = h5py.File(dataset_file_path, "r")
    file_names = f.keys()

    point_clouds = []
    labels = []

    for file_name in file_names:
        pc_raw = f.get(file_name)[()]
        point_cloud = pc_raw[:, 0:3]
        label = pc_raw[:, -1]
        # Apply one-hot encoding to the dense label representation.
        label_one_hot = keras.utils.to_categorical(label, num_classes=len(LABELS))
        point_clouds.append(point_cloud)
        labels.append(label_one_hot)
    
    f.close()

    return point_clouds, labels

def load_data(point_cloud_batch, label_batch):
    point_cloud_batch.set_shape([NUM_SAMPLE_POINTS, NUM_FEATURES])
    label_batch.set_shape([NUM_SAMPLE_POINTS, len(LABELS)])
    return point_cloud_batch, label_batch

## - Simple offline data augmentation with jittering operation
def augment(point_cloud_batch, label_batch):
    pos_jittering = tf.random.uniform((tf.shape(point_cloud_batch)[0], tf.shape(point_cloud_batch)[1], 3), \
                                      -5, 5, dtype=tf.float32)
    jittering = tf.concat((pos_jittering, \
                           tf.zeros((tf.shape(point_cloud_batch)[0], tf.shape(point_cloud_batch)[1], NUM_FEATURES - 3), \
                                    dtype=tf.float32)), axis=2)
    point_cloud_batch += jittering
    return point_cloud_batch, label_batch

def generate_dataset(point_clouds, labels, is_training=True):
    dataset = tf.data.Dataset.from_tensor_slices((point_clouds, labels))
    dataset = dataset.shuffle(BATCH_SIZE * 100) if is_training else dataset
    dataset = dataset.map(load_data, num_parallel_calls=tf.data.AUTOTUNE)
    dataset = dataset.batch(batch_size=BATCH_SIZE)
    dataset = (
        dataset.map(augment, num_parallel_calls=tf.data.AUTOTUNE)
        if is_training
        else dataset
    )
    return dataset

def conv_block(x: tf.Tensor, filters: int, name: str) -> tf.Tensor:
    x = layers.Conv1D(filters, kernel_size=1, padding="valid", name=f"{name}_conv")(x)
    x = layers.BatchNormalization(momentum=0.0, name=f"{name}_batch_norm")(x)
    return layers.Activation("relu", name=f"{name}_relu")(x)


def mlp_block(x: tf.Tensor, filters: int, name: str) -> tf.Tensor:
    x = layers.Dense(filters, name=f"{name}_dense")(x)
    x = layers.BatchNormalization(momentum=0.0, name=f"{name}_batch_norm")(x)
    return layers.Activation("relu", name=f"{name}_relu")(x)


class OrthogonalRegularizer(keras.regularizers.Regularizer):
    """Reference: https://keras.io/examples/vision/pointnet/#build-a-model"""

    def __init__(self, num_features, l2reg=0.001):
        self.num_features = num_features
        self.l2reg = l2reg
        self.identity = tf.eye(num_features)

    def __call__(self, x):
        x = tf.reshape(x, (-1, self.num_features, self.num_features))
        xxt = tf.tensordot(x, x, axes=(2, 2))
        xxt = tf.reshape(xxt, (-1, self.num_features, self.num_features))
        return tf.reduce_sum(self.l2reg * tf.square(xxt - self.identity))

    def get_config(self):
        config = super(TransformerEncoder, self).get_config()
        config.update({"num_features": self.num_features, "l2reg_strength": self.l2reg})
        return config


def transformation_net(inputs: tf.Tensor, num_features: int, name: str) -> tf.Tensor:
    """
    Reference: https://keras.io/examples/vision/pointnet/#build-a-model.

    The `filters` values come from the original paper:
    https://arxiv.org/abs/1612.00593.
    """
    x = conv_block(inputs, filters=64, name=f"{name}_1")
    x = conv_block(x, filters=128, name=f"{name}_2")
    x = conv_block(x, filters=1024, name=f"{name}_3")
    x = layers.GlobalMaxPooling1D()(x)
    x = mlp_block(x, filters=512, name=f"{name}_1_1")
    x = mlp_block(x, filters=256, name=f"{name}_2_1")
    return layers.Dense(
        num_features * num_features,
        kernel_initializer="zeros",
        bias_initializer=keras.initializers.Constant(np.eye(num_features).flatten()),
        activity_regularizer=OrthogonalRegularizer(num_features),
        name=f"{name}_final",
    )(x)


def transformation_block(inputs: tf.Tensor, num_features: int, name: str) -> tf.Tensor:
    transformed_features = transformation_net(inputs, num_features, name=name)
    transformed_features = layers.Reshape((num_features, num_features))(
        transformed_features
    )
    return layers.Dot(axes=(2, 1), name=f"{name}_mm")([inputs, transformed_features])
